const DataTypes = require("sequelize");
const sequelize = require("../config/sequelize");

const User = sequelize.define('User', {
    username: {
        type: DataTypes.STRING,
        allowNull: false
    },

    name: {
        type: DataTypes.STRING,
        allowNull: false
    },

    email: {
        type: DataTypes.STRING,
        allowNull: false
    },

    idade: {
        type: DataTypes.INTEGER
    },

    password: {
        type: DataTypes.STRING
    }

}, {});

User.associate = function(models) {
    User.hasMany(models.Comment);
}

module.exports = User;
